
struct HTTPPMU {
    std::atomic_uint64_t ProcessRound = {0};
    std::atomic_uint64_t ConnectionCount = {0};
    std::atomic_uint64_t ActiveConnecionCount = {0};
    std::atomic_uint64_t RequestCount = {0};
    std::atomic_uint64_t NormalClosedCount = {0};
    std::atomic_uint64_t ErrorClosedCount = {0};
    std::atomic_uint64_t ReadingCount = {0};
    std::atomic_uint64_t WritingCount = {0};
    std::atomic_uint64_t WatingCount = {0};
};

class HTTPServer : public Server {
protected:

    HTTPConnectionBuilder ConnectionBuilder;
    EPollEventDomain EventDomain;
    HTTPParser Parser;
    HTTPPMU PMU;

    virtual RuntimeError PostProcessFinished();

public:
    HTTPServer(int ThreadCount, int EPollSize, size_t BufferBlockSize,
               uint64_t ConnectionRecycleSize, uint64_t BufferRecycleSize);

    virtual EventError AttachListening(HTTPListening &L);

    virtual EventError DetachListening(HTTPListening &L);

    virtual RuntimeError GetConnection(HTTPConnection *&C, int SocketFD, SocketAddress &Address);

    virtual RuntimeError PutConnection(HTTPConnection *&C);

    RuntimeError ServerEventProcess();

    ~HTTPServer() = default;
};
