namespace ngx {
    namespace HTTP {
        namespace DefaultConfig {
            const bool ALLOW_UNDERSCORE = true;
            const uint64_t CONNECTION_RECYCLE_WAIT_TIME = 10000000;   // us
        }
    }
}
