//===- Connection.h - represent a Connection -----*- C++ -*-===//
//
//                     The NGX Server Infrastructure
//
// This file is distributed under the MIT Open Source License. See LICENSE.TXT
// for detail.
//
//===-------------------------------------------------------------------------===//
//
//  This file declare the Connection basic class
//
//===-------------------------------------------------------------------------===//


class Connection : public Socket {
protected:
public:
    Connection();

    Connection(struct SocketAddress &SocketAddress);

    Connection(int SocketFD, struct SocketAddress &SocketAddress);

    virtual SocketError Connect() = 0;

    virtual SocketError Close() = 0;
};


class TCP4Connection : public Connection {
public:
    TCP4Connection(struct SocketAddress &SocketAddress);

    TCP4Connection(int SocketFD, struct SocketAddress &SocketAddress);

    virtual SocketError Connect();

    virtual SocketError Close();
};
