class CanGC {
public:
    virtual void GC() = 0;
};
