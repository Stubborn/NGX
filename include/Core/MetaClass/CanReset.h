class CanReset {
public:
    virtual void Reset() = 0;
};
