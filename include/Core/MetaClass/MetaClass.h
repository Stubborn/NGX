namespace ngx {
    namespace Core {
        namespace MetaClass {

#include "Core/MetaClass/Achor.h"
#include "Core/MetaClass/AlignBuild.h"
#include "Core/MetaClass/GC.h"
#include "Core/MetaClass/Allocator.h"
#include "Core/MetaClass/AllocatorBuild.h"
#include "Core/MetaClass/Reference.h"
#include "Core/MetaClass/CanReset.h"
#include "Core/MetaClass/Lock.h"
        }

    }
}