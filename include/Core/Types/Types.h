
namespace ngx {
    namespace Core {
        namespace Types {
            using namespace ngx::Core::MetaClass;
            using namespace ngx::Core::DefaultConfig;

#include "Core/Types/Array.h"
#include "Core/Types/Queue.h"
#include "Core/Types/List.h"
#include "Core/Types/RBTree.h"

        }
    }
}
